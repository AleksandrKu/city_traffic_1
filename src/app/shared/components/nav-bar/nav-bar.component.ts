/**
 * Created by User on 15.06.2017.
 */
export * from "./providers/nav-bar.service.provider";
export * from "./nav-bar.module";
export { NavBarServiceImplementation as NavBarService } from "./services/nav-bar.service";