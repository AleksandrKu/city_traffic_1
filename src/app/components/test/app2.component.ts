/*import "./app.scss";*/
export class AppController2 {
   /*static $inject = ["$rootScope"];*/
    /*private leftSideNavId: string = "leftNav";*/
    constructor(
        private $rootScope: ng.IScope,
    ) { }
}

export const App2 = {
    selector: "app2",
    controller: AppController2,
    controllerAs: "App2",
    template: require("./app2.html"),
    bindings: {},
};
