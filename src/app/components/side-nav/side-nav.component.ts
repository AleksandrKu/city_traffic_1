import "./side-nav.scss";

class SideNavController {
/*        onClick(value) {
        console.log(value);
    }*/
    constructor() {
        /*console.log("I am constructor");*/
    }
}

export const SideNav = {
    selector: "sideNav",
    controller: SideNavController,
    controllerAs: "SideNav",
    template: require("./side-nav.html"),
    bindings: {
        componentId: "@",
    }
};
