export class TestController {
    static $inject = ["$rootScope"];
    private leftSideNavId: string = "leftNav";
    constructor(
        private $rootScope: ng.IScope,
    ) { }
}

export const Test = {
    selector: "test",
    controller: TestController,
    controllerAs: "Test",
    template: require("./test.html"),
    bindings: {},
};
