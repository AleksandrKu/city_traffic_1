import * as angular from "angular";
import { App } from "./app.component";
/*import { App } from "./app.component";*/
import { TopBar, SideNav } from "./components";

import { materialConfig } from "./config";

import { StatesModule } from "./states";
import { routes } from "./app.route";

import "./app.scss";

angular.module("app", [
    "ui.router",
    "ngMaterial",
    "ngMdIcons",
    "ngMap",
    StatesModule.name,
]).config(routes)
    .config(materialConfig)
    .component(TopBar.selector, TopBar)
    .component(App.selector, App)
    .component(SideNav.selector, SideNav);

angular.bootstrap(document.getElementById("app"), ["app"]);