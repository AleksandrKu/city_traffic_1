/**
 * Created by User on 15.06.2017.
 */
import * as angular from "angular";

export const routesConfig = ($locationProvider: angular.ILocationProvider) => {
    $locationProvider.hashPrefix("");
};